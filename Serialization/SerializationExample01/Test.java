package Serialization.SerializationExample01;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

public class Test {

    public static void main(String[] args) throws IOException, ClassNotFoundException {

        String name, type;
        name = "";
        type = "";
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter name:\t");
        name = scanner.next();
        System.out.println("Enter anime type (Series,OVA,Film)");
        type = scanner.next();

        System.out.println("The anime " + name + " with type " + type + ""
                + " was serialized");

        var anime = new SeriAnime(name, type);

        System.out.println("anime " + anime.toString());

        // Serializacion
        FileOutputStream outstream = new FileOutputStream("Anime.javaobj");
        ObjectOutputStream objectout = new ObjectOutputStream(outstream);
        objectout.writeObject(anime);
        objectout.close();
        outstream.close();
        scanner.close();


        // Deserializacion
        var inputStream = new FileInputStream("Anime.javaobj");
        var objectinputStream = new ObjectInputStream(inputStream);

        var readAnime = (SeriAnime) objectinputStream.readObject();

        inputStream.close();
        objectinputStream.close();
        System.out.println("\n\n\n\n");
        System.out.println("Anime deserialized");
        System.out.println(readAnime);

    }
}
