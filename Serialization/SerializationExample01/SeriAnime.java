package Serialization.SerializationExample01;

import java.io.Serializable;

public class SeriAnime implements Serializable {
    private String name;
    private String type;

    public SeriAnime(String name,String type)
    {
        this.name = name;
        this.type = type;
    }

    public void setName(String name)
    {
        this.name = name;
    }
    public void setType(String type)
    {
        this.type = type;
    }
    @Override
    public String toString() {
        
        return "Anime [name " + this.name + " type " + this.type +" ]\n";
    
    }
}
