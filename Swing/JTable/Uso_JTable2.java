/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author jorge
 */
public class Uso_JTable2 {

    public static void main(String[] args) {
        JFrame mimarco = new MarcoTablaPersonalizado();
        mimarco.setVisible(true);
        mimarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}

class MarcoTablaPersonalizado extends JFrame {

    public MarcoTablaPersonalizado() {
        setTitle("Tabla personalizada");
        setBounds(300, 300, 400, 200);
        
        TableModel mimodelo = new ModeloTablaPersonalizada();
        
        JTable mitabla = new JTable(mimodelo);
        mitabla.getTableHeader().setReorderingAllowed(false);
        add(new JScrollPane(mitabla));
    }
}


class ModeloTablaPersonalizada extends AbstractTableModel
{
 @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public int getRowCount() {
        return 5;
    }

   
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return null;
    }
    
    @Override
    public String getColumnName(int c)
    {
        return "Columna " + c;
    }
}
