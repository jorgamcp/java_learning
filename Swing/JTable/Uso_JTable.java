/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 *
 * @author jorge
 */
public class Uso_JTable {
    
    public static void main(String[] args) {
        JFrame mimarco = new MarcoTabla();
        mimarco.setVisible(true);
        mimarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
    }
}

class MarcoTabla extends JFrame {
    
    private String[] nombresColumnas = {"Nombre", "Radio", "Lunas", "Gaseoso"};
    private Object[][] datosFila
            = {
                {"Mercurio", 2440.0, 0, false},
                {"Venus", 6052.0, 0, false},
                {"Tierra", 6378.0, 0, false},
                {"Marte", 2440.0, 2, false},
                {"Jupiter", 71492.0, 16, true},
                {"Saturno", 60268.0, 18, true},
                {"Urano", 25559.0, 17, true},
                {"Neptuno", 24766.0, 8, true},
                {"Pluton", 1137.0, 1, false},};
    
    public MarcoTabla() {
        setTitle("Los planetas");
        setBounds(300, 300, 400, 200);
        
        JTable tablaPlanetas = new JTable(datosFila, nombresColumnas);
        tablaPlanetas.getTableHeader().setReorderingAllowed(false);
        add(new JScrollPane(tablaPlanetas), BorderLayout.CENTER);
        
        JButton botonImprimir = new JButton("Imprimir tabla");
        
        botonImprimir.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
                try {
                    tablaPlanetas.print();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                
            }
        });
        
        JPanel laminaBoton = new JPanel();
        laminaBoton.add(botonImprimir);
        add(laminaBoton, BorderLayout.SOUTH);
    }
}
