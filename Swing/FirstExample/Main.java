import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import java.awt.*;
import java.awt.event.*;

public class Main {
    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
        
        String windows_style = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";

        String motif_stlye = "com.sun.java.swing.plaf.motif.MotifLookAndFeel";
        UIManager.setLookAndFeel(motif_stlye);
        JFrame frame = new JFrame();
        JPanel panel = new JPanel();
        JPanel panel2 = new JPanel();
        JTextArea myJTextArea = new JTextArea(10, 30);
        JButton sendButton = new JButton("Send");
        JLabel title = new JLabel("Swing Example Project",SwingConstants.CENTER);
        Color color1 = Color.getHSBColor(128,131,144);
        frame.setSize(400, 350);
        frame.setTitle("Swing Example Project - Java App");

        panel.setLayout(new BorderLayout());
        panel2.setLayout(new FlowLayout());


        title.setFont( new Font("Arial",Font.BOLD,25));
        panel.add(title, BorderLayout.NORTH);

        myJTextArea.setFont(new Font("Consolas", Font.BOLD, 15));
        //808390
        myJTextArea.setBorder(BorderFactory.createCompoundBorder(myJTextArea.getBorder(), BorderFactory.createEtchedBorder(color1,color1)));
        myJTextArea.setLineWrap(true);
        panel2.add(myJTextArea);
            


        sendButton.addActionListener( new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(sendButton, "Esto es un mensaje to chulo porque has apretao el button", "INFO", JOptionPane.INFORMATION_MESSAGE);
            }
        });


        panel.add(panel2, BorderLayout.CENTER);
        panel.add(sendButton, BorderLayout.SOUTH);
        frame.add(panel);
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}