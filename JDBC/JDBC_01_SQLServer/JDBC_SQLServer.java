package JDBC.JDBC_01_SQLServer;

import java.sql.*;

public class JDBC_SQLServer {
    
    public static void main(String[] args) {
        System.out.println("JDBC with SQL Server!");

        String connectionString  ="jdbc:sqlserver://KAOS;Database=EmpresaDB;IntegratedSecurity=true;trustServerCertificate=true";

        try (Connection connection = DriverManager.getConnection(connectionString)) {
            System.out.println("Connected to SQL Server:\t"+ connection.getMetaData().getDatabaseProductName() + " Version " + 
            connection.getMetaData().getDatabaseProductVersion());


            System.out.println(connection.getCatalog());
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT nombre,apellido1,apellido2,FechaNacimiento FROM EMPLEADOS");

            ResultSet rs = preparedStatement.executeQuery();
            while(rs.next())
            {
                System.out.println("DNI : " + rs.getString(1) + " " + 
                "Nombre:  " + rs.getString(2) + " " + 
                "Apellido: " + rs.getString(3) + " " +
                "Segundo Apellido: "  + rs.getString(4) + " " + 
                "Fecha Nacimiento: " + rs.getDate(4)+ " ");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
      
    }
}
