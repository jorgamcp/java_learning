package Files.AccesoFicheros;

import java.io.FileReader;
import java.io.IOException;

public class Acceso_Fichero {
    public static void main(String[] args) {

        var accediendo = new Leer_Fichero();

        accediendo.lee();
    }
}

class Leer_Fichero {

    public Leer_Fichero() {

    }

    public void lee() {
        try {
            FileReader entrada = new FileReader("D:\\programacion\\ProgramasJava\\Files\\AccesoFicheros\\ejemplo.txt");

            int c = entrada.read();
            char letra = (char) c;
            while (c != -1) {
                System.out.print(letra);
                c = entrada.read();
                letra = (char) c;
            }
        } catch (IOException e) {
            System.out.println("No se ha encontrado el archivo.");
        }
    }
}