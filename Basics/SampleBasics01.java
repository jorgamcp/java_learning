package Basics;

import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class SampleBasics01 {
    public static void main(String[] args) {
        
        /*
            Create a Java Program that ask the user 3 names then order by alphab
            
         */

        String name = "";
        Scanner scanner = new Scanner(System.in);
        
        // This is an Array of String
        String[] names = new String[3];
        
        int step = 0;
        do {
            System.out.println("Enter a name\t");
            name = scanner.nextLine();
            names[step] = name;
            step = step + 1;
        }while(step <= 2);


        System.out.println("Entered names are:\t");
        for(String n : names)
        {
            System.out.println(n);
        }
        System.out.println();
        System.out.println("Order list names:\t");

        Arrays.sort(names);
        for(String ord_name : names)
            System.out.println(ord_name);
            
        Arrays.sort(names,Collections.reverseOrder());
        
        for(String ord_name : names)
            System.out.println(ord_name);
            

        scanner.close();

    }
}
