package Basics;

public class Variables {
    public static void main(String[] args) {
        System.out.println("Variables");

        int integer = 20;
        float floatantes = 20.3f;
        double decimals = 20.3992938;
        short n = 2;
        byte b = 0;
        boolean isBoolean = true;
        String my_string = "This is a String variable";
        char character = 'B';

        System.out.println("INT " + integer);
        System.out.println("FLOAT " + floatantes);
        System.out.println("DOUBLE " + decimals);
        System.out.println("SHORT " + n);
        System.out.println("BYTE " + b);
        System.out.println("BOOLEAN " + isBoolean);
        System.out.println("STRING " + my_string);
        System.out.println("CHARACTER " + character);
        
    }
}
