package Basics.Arrays;

public class ArraysIntro {
    public static void main(String[] args) {
        System.out.println("Arrays Java First Example / ArraysIntro.java");

        // An array is a variable with multiple values in the same contiguous memory
        int[] numbers = new int[9]; // Creates an array of 10 positions because, in java arrays start in 0 index. 0
                                    // to 9 is 10 positions
        int index = 0; // Let's keep index in an higher scope of for loop to use it with do/while while
                       // loops

        // Assign values to each postion of the array name_array[position_index] = value;
        numbers[0] = 30;
        numbers[1] = 12;
        numbers[2] = 8;
        numbers[3] = 3;
        numbers[4] = 14;
        numbers[5] = 100;
        numbers[6] = 39;
        numbers[7] = 78;
        numbers[8] = 1000;

        // Prints the array with do while loop
        System.out.println("Printed Array with do/while loop:");
        do {
            System.out.println(numbers[index]);
            index = index + 1;
        } while (index < 9);
        System.out.println("Now index's value variable is " + index + " after do while loop");
        // Print the array with while loop
        index = 0; // reset index because here index in previous loop is equals to 9
        System.out.println("index value has been reset to  " + index + "\nPrinted Array with while loop:");
        while (index < 9) {
            System.out.println(numbers[index]);
            index++;
        }
        // Print the array in the console or more exactly the standart ouput. using a
        // classic for loop
        System.out.println("Printed Array with classic for loop:");
        for (index = 0; index < 9; index++) // for(declaration;condition;increment)
        {
            System.out.println(numbers[index]);
        }

        // Enhanced for loop
        System.out.println("Printed Array with enhanced for loop");
        for (int number : numbers) {
            System.err.println(number);
        }

    }
}
