package Basics.Arrays;

import java.util.Arrays;
import java.util.Collections;

public class ArraysIntro4 {
    public static void main(String[] args) {
        // Declaración e inicialización en la misma linea

        String[] animes = new String[] { "Sangri-La Frontier: Kusoge Hunter,Kamige ni Idoman to su",
                "Sousou No Frieren",
                "Kusuriya No Hitorigoto",
                "Ore dake Level Up na Ken",
                "Megumi no Daigo: Kyuukoku no Orange",
                "Mahou Shoujo ni Akkogarete",
                "Undead Unluck"};

        System.out.println("Los animes que estoy viendo esta temporada (Invierno 2024) son:");    
        for (String anime_name : animes) {
            System.out.println(anime_name);
        }
        /*
         * La propiedad .length me devuelve la longitud del array.
         */
        System.out.println("En total son " + animes.length + " animes.");
        System.out.println("\n");
        System.out.println("Ordeno los animes alfabéticamente:");
        
        Arrays.sort(animes); // Metodo estático de la clase Arrays ordena Alfabéticamente de A-Z
        Arrays.sort(animes,Collections.reverseOrder());

        for (int i = 0; i < animes.length; i++) {
            System.out.println(animes[i]);
        }

        System.out.println("\nEl array original ha mutado, es decir, ha cambiado.\n");
        for(String anime_name : animes)
        {
            System.out.println(anime_name);
        }

    }

}
