package Basics.Arrays;

public class ArraysIntro2 {
    public static void main(String[] args) {

        // Second program in Java to learn arrays
        int[] my_numbers = new int[4];

        my_numbers[0] = 31;
        my_numbers[1] = 32;
        my_numbers[2] = 33;
        my_numbers[3] = 34;

        // Si el bucle for sólo tiene una instrucción, se puede poner sin llaves y con una tabulación
        for(int i = 0; i < my_numbers.length;i++)
            System.out.println("En la posición " + i + " el elemento es " + my_numbers[i]);

        System.out.println("\nAhora recorremos el array al revés\n");
        
        // Recorremos el array al revés
        for(int i = my_numbers.length-1;i >= 0;i--)
            System.out.println("En la posición " + i + " el elemento es " + my_numbers[i]);
        System.out.println();

        int[] otherArray = {15,25,85,-34,11,10};
        
        for(int i = 0; i < otherArray.length;i++)
        {
            System.out.printf("En la posición %d, el elemento es %s",i,otherArray[i]+"\n");
        }
    }
}
