package Basics.Arrays;

public class ArraysIntro3 {
    public static void main(String[] args) {

        int[] marks = new int[4]; // Declaración e inicialización en la misma linea

        // Declaración e inicialización en diferentes líneas.
        String[] students;
        students = new String[4];

        marks[0] = 2;
        students[0] = "Juan";

        marks[1] = 4;
        students[1] = "Sofia";

        marks[2] = 5;
        students[2] = "Maria";

        marks[3] = 7;
        students[3] = "Alberto";

        for(int i = 0; i < marks.length;i++)
        {
            System.out.println("Student:\t"+students[i] + " Mark:\t"+marks[i]);
        }
    }
}
