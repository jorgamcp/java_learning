package Basics.Arrays;

public class BucleForMejorado {
    public static void main(String[] args) {

        String[] countries = new String[9];
        countries[0] = "Spain";
        countries[1] = "Venezuela";
        countries[2] = "France";
        countries[3] = "Italy";
        countries[4] = "Colombia";
        countries[5] = "Japan";
        countries[6] = "China";
        countries[7] = "North Korea";
        countries[8] = "South Korea";

        // Enhanced for loop

        for (String name : countries) {
            System.out.println("Country: " + name);
        }
    }
}
