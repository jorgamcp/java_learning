package Basics;

import java.util.Scanner;

public class Loops_DoWhile {
    public static void main(String[] args) {
        System.out.println("A program in java about Loops ");

        int times = 3;
        int user_input = 0;
        int t_user_input = 0; // Accumulator variable
        Scanner scanner = new Scanner(System.in);
        do {
            System.out.println("Enter a number: \t");
            user_input = scanner.nextInt(); // reads an integer.
            t_user_input = t_user_input + user_input;
            times = times - 1;
        }while(times > 0);


        System.out.println("The average sum is " + t_user_input);

        // Close scanner
        scanner.close();
    }
}
