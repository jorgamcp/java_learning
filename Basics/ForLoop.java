package Basics;

import java.util.Scanner;
public class ForLoop {
    public static void main(String[] args) {
        
        System.out.println("Example of for loop");

        Scanner scanner = new Scanner(System.in);

        int number = 0;

        System.out.println("Enter a number:\t");
        number = scanner.nextInt();

        for(int counter = 0; counter < 11; counter++)
        {
            System.out.println( number + " X " + counter + " = " + (number*counter));
        }

        scanner.close();
    }
}
