package Basics;

import java.util.Scanner;

public class Loops_While {
    public static void main(String[] args) {
        
        // A program shows While loop in java

        Scanner scanner = new Scanner(System.in);

        int number = 0,counter = 0; // In Java can declare multiple variables of the same time in the same line.


        System.out.println("Enter a number:\t");

        number = scanner.nextInt(); // reads as an integer

        while (counter < 11)
        {
            System.out.println(number + " X " + counter + " = " + (number*counter));
            counter++; // is equivalent to counter = counter + 1;
        }


        scanner.close();

    }
}
