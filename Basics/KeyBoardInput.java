package Basics;

import java.util.Scanner; // Scanner class is in java.util package. To use this class i need to import this package otherwise will fail and don't compile

public class KeyBoardInput {
    public static void main(String[] args) {
        // A program that illustres input in Java.

        /* Ask the user their name and prints out a hello message to the user */
       
        System.out.println("What's your name?\t"); // \t prints a tabulation

        // We use the class 'Scanner' to create a new instance of this class
        Scanner scanner = new Scanner(System.in); // system.in is the standard input in this case the Keyboard

        // Declare a variable to store the name initialized empty string
        String name = "";

        // read the name of input user and assign to variable name 
        name = scanner.nextLine(); 

        // Print the hello message to the user 
        System.out.println("Hello " + name + "!");

        // Scanner is a stream System.in is an InputStream so i need to close

        scanner.close();

    }
}
